function createNewUser() {
    return {
        _firstName: prompt("enter first name!"),
        _lastName: prompt("enter second name!"),
        _birthday: prompt("enter birthday in next format (dd.mm.yyyy)!"),
        get login () {
            return (this._firstName.charAt(0) + this._lastName).toLowerCase();
        },
        set firstName (value) {
            return this._firstName = value;
        },
        set lastName (value) {
            return this._lastName = value;
        },
        get age () {
            const dayOfBirthday = Number(this._birthday.split('.')[0]);
            const monthOfBirthday = Number(this._birthday.split('.')[1]);
            const currentDate = new Date();
            const birthDate = new Date(currentDate.getFullYear(), monthOfBirthday - 1, dayOfBirthday);
            if (birthDate > currentDate) {
                return `User age is ${currentDate.getFullYear() - this._birthday.split('.')[2] - 1}`;
            } else return `User age is ${currentDate.getFullYear() - this._birthday.split('.')[2]}`;
        },
        get password () {
            return `User password is ${this._firstName.charAt(0).toUpperCase() + this._lastName.toLowerCase() + this._birthday.split('.')[2]}`;
        }
    }
}



let someUser = createNewUser();
console.log(someUser.password);
console.log(someUser.age);
