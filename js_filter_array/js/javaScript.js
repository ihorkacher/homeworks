const testArray = ['3,14', [], 'John', 4242, 'Michael', true, 'Margo', 'Ihor', false, null, 42, {}, 422];


function filterBy(arr, datType) {
    const newArr = [];
    let j = 0;
    if (datType === null) {
        for (let arrItm of arr) {
            if (!(arrItm === null && typeof arrItm === typeof datType)){
                newArr[j] = arrItm;
                j++;
            }
        }
        return newArr;
    } else
        for (let arrItm of arr) {
            if (typeof arrItm !== typeof datType) {
                newArr[j] = arrItm;
                j++;
            }
        }
        return newArr;
}

console.log(filterBy(testArray, null)); // without null
console.log(filterBy(testArray, 'string')); // without string
console.log(filterBy(testArray, 42)); // without number
console.log(filterBy(testArray, true)); // without boolean
console.log(filterBy(testArray, [])); // without object (arr & obj)

