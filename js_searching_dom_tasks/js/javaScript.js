// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphList = document.querySelectorAll('p');

paragraphList.forEach(paragraph => {
    paragraph.style.opacity = "1"
    paragraph.style.background = "#f00000"
});


// Знайти елемент з id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const elementOptionsList = document.getElementById('optionsList');
console.log(elementOptionsList);

const elementOptionsListParent = elementOptionsList.parentElement;
console.log(elementOptionsListParent);

if (elementOptionsList.hasChildNodes()) {
    element = elementOptionsList.childNodes;
    for (let i of element) {
        console.log(i.nodeName);
        console.log(i.nodeType);
    }
} else {
    console.log("No any Nodes!");
}



// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
let contentTestParagraph = document.getElementById("testParagraph");
contentTestParagraph.innerText = "This is a paragraph";



// Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header').children;
console.log(mainHeader);

for (let element of mainHeader) {
    element.classList.add('nav-item');
}


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

const sectionTitleList = document.querySelectorAll('.section-title');
console.log(sectionTitleList);

sectionTitleList.forEach(section => {
    section.classList.remove('section-title');
});
console.log(sectionTitleList);

