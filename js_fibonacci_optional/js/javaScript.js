function fibonacci (number, firstNum, secondNum) {
    if (number === 2){
        return secondNum;
    } else if (number === 1){
        return firstNum;
    } else {
        return fibonacci (number - 1, firstNum, secondNum) + fibonacci (number - 2, firstNum, secondNum);
    }
}
function fibonacciMinus (number, firstNumb,secondNum) {
    if (number === 2){
        return secondNum;
    } else if (number === 1){
        return firstNumb;
    } else {
        return fibonacciMinus (number - 2, firstNumb, secondNum) - fibonacciMinus (number - 1, firstNumb, secondNum);
    }
}



let firstNumber = +prompt('Enter FIRST number');
while(isNaN(firstNumber)) {
    firstNumber = +prompt('Enter FIRST number AGAIN', '');
}
let secondNumber = +prompt('Enter SECOND number');

while(isNaN(secondNumber)) {
    secondNumber = +prompt('Enter SECOND number AGAIN', '');
}
let number = +prompt('Enter number:');
while(isNaN(number)) {
    number = +prompt('Enter number AGAIN', '');
}

if (firstNumber > 0 && secondNumber > 0){
    alert(`Value of Fibonacci number is: ${fibonacci(number, firstNumber, secondNumber)}`);
} else {
    alert(`Value of Fibonacci number is: ${fibonacciMinus(number, firstNumber, secondNumber)}`);
}