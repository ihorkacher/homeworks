import { books } from './books.js';

const root = document.getElementById("root");
const ul = document.createElement("ul");

books.forEach((book, idx)=> {
    const requiredFields = ['author', 'name', 'price'];
    const bookFields = Object.keys(book);

    try {
        requiredFields.forEach(field => {
            if (!bookFields.includes(field)) {
                throw new Error(`Відсутнє поле: ${field} : масив books[${idx}]`);
            }
        });

        const { author, name, price } = book;
        const li = document.createElement("li");
        li.textContent = `${author}, "${name}", ${price} грн`;
        ul.appendChild(li);
    } catch (error) {
        console.error(error);
    }
});

root.append(ul);


