
window.addEventListener("load", windowLoad);

function windowLoad() {
    const mainTheme = document.querySelector(".main-theme");
    const darkTheme = document.querySelector(".dark-theme");
    const themeBtn = document.querySelector(".theme-btn");
    let themeBtnIconList = document.querySelectorAll(".theme-btn div");
    const nightIcon = themeBtnIconList[0];
    const dayIcon = themeBtnIconList[1];

    if (localStorage.userTheme === "darkTheme"){
        nightIcon.id = 'null';
        dayIcon.id = 'active';
        mainTheme.setAttribute("href", "null");
        darkTheme.setAttribute("href", "css/css_dark_theme.css");
    } else {
        nightIcon.id = 'active';
        dayIcon.id = 'null';
        darkTheme.setAttribute("href", "null");
        mainTheme.setAttribute("href", "css/style.css");
    }



    themeBtn.addEventListener("click", () => {

        themeBtnIconList.forEach((icon, idx, iconList) => {
            if (icon.id === 'active') {
                nightIcon.id = 'null';
                dayIcon.id = 'active';
                mainTheme.setAttribute("href", "null");
                darkTheme.setAttribute("href", "css/css_dark_theme.css");
                localStorage.setItem("userTheme", 'darkTheme');
            } else {
                nightIcon.id = 'active';
                dayIcon.id = 'null';
                darkTheme.setAttribute("href", "null");
                mainTheme.setAttribute("href", "css/style.css");
                localStorage.setItem("userTheme", 'mainTheme');
            }
        });

    });
}


