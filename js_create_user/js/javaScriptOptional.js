function createNewUser() {
    return {
        _firstName: prompt("enter first name!"),
        _lastName: prompt("enter second name!"),

        get login () {
            return (this._firstName.charAt(0) + this._lastName).toLowerCase();
        },
        set firstName (value) {
            return this._firstName = value;
        },
        set lastName (value) {
            return this._lastName = value;
        }
    }
}


let newUser = createNewUser();
console.log(newUser);

newUser.lastName = prompt("enter new surname!");
console.log(newUser);
