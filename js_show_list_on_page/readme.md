1. Опишіть, як можна створити новий HTML тег на сторінці.
var element = document.createElement(tagName);
2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Element.insertAdjacentHTML( position, text );
position - рядок який містить позицію вставки тексту:
"beforebegin" - перед самим елементом.
"afterbegin" - всередині елемента, на початку.
"beforeend" - всередині елемента, в кінці.
"afterend" - після самим елементом.

3. Як можна видалити елемент зі сторінки?
Старий метод:
let element = document.getElementById('element');
element.parentNode.removeChild(element);
Новий метод:
const element = document.getElementById('lement');
element.remove();
