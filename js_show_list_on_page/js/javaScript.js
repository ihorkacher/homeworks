const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

listCreate = (arr, parentTagName = document.body) => {
    if (parentTagName !== document.body) {
        let element = document.createElement(`${parentTagName}`);
        document.body.append(element);
        let list = listCreate(arr);
        element.append(list);

    } else {
        let ul = document.createElement('ul');
        parentTagName.append(ul);
        arr.map((value, index, array) => {
            let li = document.createElement('li');
            li.innerHTML = value;
             ul.append(li);
        })
        return ul;
    }
}

listCreate(arr);
// listCreate(arr, 'div');