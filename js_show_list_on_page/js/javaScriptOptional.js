let timer = document.createElement('div');
timer.classList.add('timer');
document.body.append(timer);
let time = 3;
timer.innerHTML = "00:0" + time;
time--;


setInterval(updateTimer = () => {
    if (time >= 0) {
        let seconds = time % 60;
        seconds = seconds < 10 ? "00:0" + seconds : seconds;
        timer.innerHTML = `${seconds}`;
        time--;
    }
}, 1000);


const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

listCreate = (arr, parentTagName = document.body) => {
    if (parentTagName !== document.body) {
        let element = document.createElement(`${parentTagName}`);
        element.classList.add('list-wrapper');
        document.body.append(element);
        let list = listCreate(arr);
        element.append(list);
        return element;
    } else {
        let ul = document.createElement('ul');
        ul.classList.add('list');
        parentTagName.append(ul);
        arr.map((value, index, array) => {
            let li = document.createElement('li');
            ul.append(li);
            if (Array.isArray(value)) {
                let childList = listCreate(value);
                li.append(childList);
            } else {
                li.innerHTML = value;
            }
        });
        return ul;
    }

}

let list = listCreate(arr, 'div');


setTimeout( () => {
    list.remove();
}, 3000);


















