let firstNum = prompt("Enter first number!");
let secondNum = prompt("Enter second number!");
let operationSymbol = prompt("Enter operation symbol!");

while (!firstNum || !secondNum || !operationSymbol || isNaN(firstNum) || isNaN(secondNum)) {
    if (firstNum === null) firstNum = '';
    if (secondNum === null) secondNum = '';
    if (operationSymbol === null) operationSymbol = '';

    firstNum = prompt("Enter first number again!", firstNum);
    secondNum = prompt("Enter second number again!", secondNum);
    operationSymbol = prompt("Enter operation symbol again!", operationSymbol);
}

const calculation = function (firstNum, secondNum, symbol) {
    firstNum = Number(firstNum);
    secondNum = Number(secondNum);
    switch (symbol) {
        case '+' :
            return firstNum + secondNum;
        case '-' :
            return firstNum - secondNum;
        case '*' :
            return firstNum * secondNum;
        case '/' :
            return firstNum / secondNum;
        default:
            return 'Something went wrong!';
    }
}
console.log(calculation(firstNum, secondNum, operationSymbol));