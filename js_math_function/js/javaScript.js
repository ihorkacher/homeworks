let firstNum = +prompt("Enter first number!");
let secondNum = +prompt("Enter second number!");
let operationSymbol = prompt("Enter operation symbol!");

const calculation = function (firstNum, secondNum, symbol) {
    switch (symbol) {
        case '+' :
            return firstNum + secondNum;
        case '-' :
            return firstNum - secondNum;
        case '*' :
            return firstNum * secondNum;
        case '/' :
            return firstNum / secondNum;
        default:
            return 'Something went wrong!';
    }
}
console.log(calculation(firstNum, secondNum, operationSymbol));
