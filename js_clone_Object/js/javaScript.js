'use strict';


const testObject = {
    name: "Jack",
    laptopList: {
        lenovo: 1,
        dell: null,
        msi: null,
        someSome: { first:1, second:2}
    },
    country: "Ukraine",
    ide: "ws",
    skillList: [["js", "jquery"], ["css"], ["html"]],
    sayHi: function () {
        console.log("Hello");
    }
}



function objectCopy (object) {
    const newObject = {};
    for (let key in object) {
        if (Array.isArray(object[key])) {
            newObject[key] = object[key].slice(0);
        } else if (typeof object[key] === "object" && !Array.isArray(object[key]) && object[key] !== null) {
            newObject[key] = objectCopy(object[key]);
        } else if (object[key] === null) {
            newObject[key] = null;
        } else newObject[key] = object[key];
    }
    return newObject;
}






const newTestObject = objectCopy(testObject);

console.log(testObject);
console.log(newTestObject);


