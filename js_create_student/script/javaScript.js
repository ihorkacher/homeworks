function createStudent(studentArg) {


    studentArg.name = prompt("Enter student name!", "Ihor");

    while (!isNaN(studentArg.name)) {
        alert("Something went wrong! Enter student name again!")
        studentArg.name = prompt("Enter student name!", "Ihor");
    }


    studentArg.lastName = prompt("Enter student lastname", "Kacher");

    while (!isNaN(studentArg.lastName)) {
        alert("Something went wrong! Enter student lastname again!")
        studentArg.lastName = prompt("Enter lastname name!", "Kacher");
    }

    return studentArg;
}

function createStudentTabel(studentArg) {


    let tabel = {}, subject, mark;

    while (confirm("Add subject?")) {

        subject = prompt("Subject");


        while (!isNaN(subject)) {
            alert("Something went wrong! Enter subject again!");
            subject = prompt("Subject!");
        }


        mark = +prompt("mark");

        while (!Number.isInteger(mark) || Number(mark) < 1 || Number(mark) > 12) {
            alert("Something went wrong! Enter mark again!");
            mark = +prompt("mark!");
        }

        tabel[subject] = mark;

    }

    return studentArg.tabel = tabel;
}

function numberOfLowMarks (studentArg) {


    let markCounter = 0;

    for (const subject in studentArg.tabel) {
        if (studentArg.tabel[subject] < 4) {
            markCounter ++;
        }
    }

    return markCounter;
}

function averageMark(studentArg) {

    let pointsSum = 0;
    let subjectCounter = 0;

    for (const subject in studentArg.tabel) {
        pointsSum += Number(studentArg.tabel[subject]);
        subjectCounter ++;
    }

    return pointsSum / subjectCounter;
}



const student = {
    name: undefined,
    lastName: undefined,
}

createStudent(student);


if (confirm("Create tabel")) {

    createStudentTabel(student);

    const studentScholarship = numberOfLowMarks(student) <= 0 ?
        "The student has been transferred to the next course" :
        "The student has not been transferred to the next course";


    const studentAverageMark = averageMark(student) > 7 ?
        `The student is awarded a scholarship! Average mark is - ${averageMark(student)}` :
        `The student has not been awarded a scholarship! Average mark is - ${averageMark(student)}`;


    if (Object.entries(student.tabel).length !== 0) {

        if (confirm("Show scholarship result?")) {
            console.log(studentScholarship);
        }

        if (confirm("Show average tabel mark?")) {
            console.log(studentAverageMark);
        }

    }
    console.log(student);
    console.log("Tabel is empty!");
} else {
    console.log(student);
    console.log("Tabel not created!");
}





