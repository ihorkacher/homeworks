import {characters, clients1, clients2, user1, satoshi2020, satoshi2019, satoshi2018, books, bookToAdd, employee, array} from "./dataBase.js";


const mergedClientList = [...clients1, ...clients2].reduce((acc, client) => {
    if (!acc.includes(client)) acc.push(client);
    return acc;
}, []);
console.log(mergedClientList);


const charactersShortInfo = characters.map(({name, lastName, age}) => ({name, lastName, age}));
console.log(charactersShortInfo);


const { name: name, years: age, isAdmin = false } = user1;
console.log(name);
console.log(age);
console.log(isAdmin);


const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
console.log(fullProfile);


const newBooks = [...books, bookToAdd];
console.log(newBooks);


const { forename, surname} = employee;
const newEmployee = {
    ...employee,
    age: 30,
    salary: 5000
};
console.log(newEmployee);


const [ value, showValue ] = array;
alert(value);
alert(showValue());




