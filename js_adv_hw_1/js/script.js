//1
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }


//2
    get name() {
        return this._name;
    }
    set name(newName) {
        return this._name = newName;
    }

    get age() {
        return this._age;
    }
    set age(newAge) {
        return this._age = newAge;
    }

    get salary() {
        return this._salary;
    }
    set salary(newSalary) {
        return this._salary = newSalary;
    }
}

const employeeFirst = new Employee("Ihor", 28, 2500);

console.log(`Name - ${employeeFirst.name}`);
console.log(`Age - ${employeeFirst.age}`);
console.log(`Salary - $${employeeFirst.salary}`);
console.log("<------------------------>");
//was changed using "set"
employeeFirst.name = "Harry";
employeeFirst.age = undefined;
employeeFirst.salary = 3000;

console.log(`Name - ${employeeFirst.name}`);
console.log(`Age - ${employeeFirst.age}`);
console.log(`Salary - $${employeeFirst.salary}`);
console.log("<------------------------>");

