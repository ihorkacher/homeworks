//3
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }


//4
    get BonusSalary() {
        return super.salary *3;
    }
}

const programmerFirst = new Programmer("Taras", 27, 3500, ["JS", "C#", "Python", "PHP"]);
console.log("Name: " + programmerFirst.name);
console.log("Age: " + programmerFirst.age);
console.log("Bonus salary: $" + programmerFirst.salary);
console.log("Bonus salary: $" + programmerFirst.BonusSalary);
console.log("Languages: " + programmerFirst.lang.join(", "));
console.log("<------------------------>");

//5
const programmer1 = new Programmer("John", 30, 50000, ["JavaScript", "Python", "Java"]);
const programmer2 = new Programmer("Kate", 25, 45000, ["C++", "Java"]);
const programmer3 = new Programmer("Tom", 35, 60000, ["JavaScript", "Python", "Ruby"]);

console.log("Programmer 1:");
console.log("Name: " + programmer1.name);
console.log("Age: " + programmer1.age);
console.log("Salary: " + programmer1.salary);
console.log("Languages: " + programmer1.lang.join(", "));
console.log("<------------------------>");
console.log("Programmer 2:");
console.log("Name: " + programmer2.name);
console.log("Age: " + programmer2.age);
console.log("Salary: " + programmer2.salary);
console.log("Languages: " + programmer2.lang.join(", "));
console.log("<------------------------>");
console.log("Programmer 3:");
console.log("Name: " + programmer3.name);
console.log("Age: " + programmer3.age);
console.log("Salary: " + programmer3.salary);
console.log("Languages: " + programmer3.lang.join(", "));


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const root = document.getElementById("root");
const list = document.createElement("ul");

books.forEach(book => {
    try {
        if (!("author" in book) || !("name" in book) || !("price" in book)) {
            console.error(`Invalid book object: ${JSON.stringify(book)}`);
            return;
        }

        const item = document.createElement("li");
        item.textContent = `${book.author}: ${book.name} - ${book.price} грн`;
        list.appendChild(item);
    } catch (err) {
        console.error(err);
    }
});

root.appendChild(list);