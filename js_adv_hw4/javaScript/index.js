// Отримати список фільмів та вивести їх на екран
fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {
        const filmsElement = document.getElementById('films');
        data.forEach(film => {
            const filmElement = document.createElement('div');
            filmElement.innerHTML = `
        <h2>Episode ${film.episodeId}: ${film.name}</h2>
        <p>${film.openingCrawl}</p>
        <button onclick="getCharacters(${film.episodeId})">Show Characters</button>
      `;
            filmsElement.appendChild(filmElement);
        });
    });

// Отримати список персонажів фільму та вивести їх на екран
function getCharacters(episodeId) {
    const charactersElement = document.getElementById('characters');
    charactersElement.innerHTML = '<div class="spinner"></div>';

    fetch(`https://ajax.test-danit.com/api/swapi/films/${episodeId}/characters`)
        .then(response => response.json())
        .then(data => {
            const characters = data.map(character => character.name);
            charactersElement.innerHTML = `
        <h3>Characters:</h3>
        <ul>
          ${characters.map(character => `<li>${character}</li>`).join('')}
        </ul>
      `;
        });
}
